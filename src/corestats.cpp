/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QCloseEvent>
#include <QToolButton>
#include <QScroller>
#include <QScrollBar>

#include <cprime/themefunc.h>

#include "pbattery.h"
#include "pdisplay.h"
#include "pdrives.h"
#include "pgeneral.h"
#include "pnetwork.h"
#include "presources.h"
#include "psysmonitor.h"
#include "corestats.h"
#include "ui_corestats.h"

#include "global.h"

InfoManager *infoMgr = nullptr;

/** We need data of the past 2 minutes, updated every 500 ms: 2 * 60 * 2 = 240 */
int MAX_DATA_POINTS = 240;

corestats::corestats(QWidget *parent): QWidget(parent)
  , ui(new Ui::corestats)
  , smi(new settings)
  , uiMode(0)
  , toolsIconSize(32, 32)
  , pageGeneral(nullptr)
  , pageDrives(nullptr)
  , pageBattery(nullptr)
  , pageNetwork(nullptr)
  , pageResources(nullptr)
  , pageSysMonitor(nullptr)
  , pageDisplay(nullptr)
{
    ui->setupUi(this);

    /** Initialize the InfoManager */
    infoMgr = new InfoManager();

    /** We need data of the past 2 minutes, updated every 500 ms: 2 * 60 * 2 = 240 */
    infoMgr->setDataCount( MAX_DATA_POINTS );

    /** We will update the data every 500 ms */
    infoMgr->setUpdateInterval( 500 );

    /** First-time update */
    infoMgr->update();

    /** InfoManager update timer */
	QTimer *timer = new QTimer(this);
    timer->setTimerType( Qt::PreciseTimer );    // Precise Timer: update every 500 ms sharp
    timer->setInterval( 500 );                 // We needs points every 500 ms.
    timer->setSingleShot( false );              // This timer keeps repeating.

    timer->callOnTimeout(
        [=]() {
            infoMgr->update();
        }
    );

    timer->start();

    loadSettings();
    startSetup();
    setupIcons();
    on_Bgeneral_clicked();
}

corestats::~corestats()
{
	delete smi;
    delete ui;
    delete pageGeneral;
    delete pageDrives;
    delete pageBattery;
    delete pageNetwork;
    delete pageResources;
    delete pageSysMonitor;
    delete pageDisplay;
    delete infoMgr;
}

/**
 * @brief Setup ui elements
 */
void corestats::startSetup()
{
    ui->menu->setVisible(false);
    ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);
	this->resize(800, 500);

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);
        ui->menu->setIconSize(toolsIconSize);
        ui->sideView->setVisible(false);
        ui->menu->setVisible(true);

        QScroller::grabGesture(ui->scrollArea, QScroller::LeftMouseButtonGesture);

        connect(ui->menu, &QToolButton::clicked, this, &corestats::showSideView);
        connect(ui->appTitle, &QToolButton::clicked, this, &corestats::showSideView);
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }
    }

    // all toolbuttons icon size in sideView
    QList<QToolButton *> toolBtns = ui->sideView->findChildren<QToolButton *>();
    for (auto b: toolBtns) {
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }

	BatteryManager batMon;
    // Hide the battery button if there is no battery
	if (not batMon.batteries().count()) {
		ui->Bbattery->setVisible(false);
	}
}

void corestats::setupIcons()
{
    ui->Bgeneral->setIcon(CPrime::ThemeFunc::themeIcon( "dialog-information-symbolic", "dialog-information", "dialog-information" ));
    ui->Bstorage->setIcon(CPrime::ThemeFunc::themeIcon( "drive-multidisk-symbolic", "drive-multidisk", "drive-multidisk" ));
    ui->Bbattery->setIcon(CPrime::ThemeFunc::themeIcon( "battery-symbolic", "battery", "battery" ));
	ui->Bnetwork->setIcon(CPrime::ThemeFunc::themeIcon("network-receive-symbolic", "network-server", "network-receive" ));
    ui->Bresource->setIcon(CPrime::ThemeFunc::themeIcon( "view-reveal-symbolic", "password-show-on", "dialog-password" ));
    ui->Bsysmonitor->setIcon(CPrime::ThemeFunc::themeIcon( "emblem-system-symbolic", "emblem-system", "emblem-system" ));
    ui->Bdisplay->setIcon(CPrime::ThemeFunc::themeIcon( "preferences-desktop-display-symbolic", "preferences-desktop-display", "preferences-desktop-display" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
}

/**
 * @brief Loads application settings
 */
void corestats::loadSettings()
{
    // get CSuite's settings
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");

    // get app's settings
    windowSize = smi->getValue("CoreStats", "WindowSize");
    windowMaximized = smi->getValue("CoreStats", "WindowMaximized");
}

void corestats::resizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event)
	ui->scrollArea->widget()->setMaximumSize(ui->scrollArea->viewport()->size().expandedTo(ui->pages->currentWidget()->sizeHint()));
}

void corestats::showSideView()
{
    if (uiMode == 2) {
        if (ui->sideView->isVisible()) {
            ui->sideView->setVisible(false);
        } else {
            ui->sideView->setVisible(true);
        }
    }
}

void corestats::pageClick(QToolButton *btn, int i, const QString& title)
{
    // first do this to get all the space for contents
    if (uiMode == 2) {
        ui->sideView->setVisible(false);
    }

    // all button checked false
	Q_FOREACH ( QToolButton *b, ui->sideView->findChildren<QToolButton *>() ) {
        if ( b ) {
            b->setChecked(false);
        }
    }

    btn->setChecked(true);
    ui->selectedsection->setText(title);
    ui->pages->setCurrentIndex(i);
    this->setWindowTitle(title + " - CoreStats");

	ui->scrollArea->verticalScrollBar()->setSliderPosition(0);
	ui->scrollArea->horizontalScrollBar()->setSliderPosition(0);
	ui->scrollArea->widget()->setMaximumSize(ui->scrollArea->viewport()->size().expandedTo(ui->pages->widget(i)->sizeHint()));
}

void corestats::on_Bgeneral_clicked()
{
    if (!pageGeneral) {
        pageGeneral = new pgeneral(this);
        ui->pGeneralLayout->addWidget(pageGeneral);
    }

    pageClick(ui->Bgeneral, Pages::General, tr("General"));
}

void corestats::on_Bstorage_clicked()
{
    if (!pageDrives) {
        pageDrives = new pDrives(this);
        ui->pDriveLayout->addWidget(pageDrives);
    }

    pageClick(ui->Bstorage, Pages::Storage, tr("Storage"));
}

void corestats::on_Bbattery_clicked()
{
    if (!pageBattery) {
        pageBattery = new pBattery(this);
        ui->pBatteryLayout->addWidget(pageBattery);
    }

    pageClick(ui->Bbattery, Pages::Battery, tr("Battery"));
}

void corestats::on_Bnetwork_clicked()
{
    if (!pageNetwork) {
        pageNetwork = new pnetwork(this);
        ui->pNetworkLayout->addWidget(pageNetwork);
    }

    pageClick(ui->Bnetwork, Pages::Network, tr("Network"));
}

void corestats::on_Bresource_clicked()
{
    if (!pageResources) {
        pageResources = new presources(this);
        // QScrollArea *base = new QScrollArea(this);
        // base->setFrameShape(QFrame::NoFrame);
        // base->setWidgetResizable(true);
        // base->setWidget(resourcePage);
        ui->pResourceLayout->addWidget(pageResources);
    }
    pageClick(ui->Bresource, Pages::Resources, tr("Resource"));
}

void corestats::on_Bsysmonitor_clicked()
{
    if (!pageSysMonitor) {
        pageSysMonitor = new pSysMonitor(this);
        ui->pSysMonitorLayout->addWidget(pageSysMonitor);
    }

	pageClick(ui->Bsysmonitor, Pages::SystemMonitor, tr("System Monitor"));
}

void corestats::on_Bdisplay_clicked()
{
    if (!pageDisplay) {
        pageDisplay = new pDisplay(this);
        ui->pDisplayLayout->addWidget(pageDisplay);
    }

    pageClick(ui->Bdisplay, Pages::Display, tr("Display"));
}


void corestats::closeEvent(QCloseEvent *cEvent)
{
    qDebug()<< "save window stats"<< this->size() << this->isMaximized();

    smi->setValue("CoreStats", "WindowSize", this->size());
    smi->setValue("CoreStats", "WindowMaximized", this->isMaximized());

    cEvent->accept();
}
