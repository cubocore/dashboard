/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>
#include <QCloseEvent>

#include "settings.h"

class QToolButton;
class pgeneral;
class pDrives;
class pBattery;
class pnetwork;
class presources;
class pSysMonitor;
class pDisplay;

namespace Ui {
    class corestats;
}

class corestats : public QWidget {
    Q_OBJECT

public:
    enum Pages {
        General = 0,
        Storage,
        Battery,
        Network,
        Resources,
        SystemMonitor,
        Display
    };

    explicit corestats(QWidget *parent = nullptr);
    ~corestats();

private slots:
    void on_Bgeneral_clicked();
    void on_Bstorage_clicked();
    void on_Bbattery_clicked();
    void on_Bnetwork_clicked();
    void on_Bresource_clicked();
    void on_Bsysmonitor_clicked();
    void on_Bdisplay_clicked();

    void showSideView();

protected:
	void resizeEvent(QResizeEvent *event) override;
    void closeEvent(QCloseEvent *cEvent) override;

private:
    Ui::corestats *ui;
    settings *smi;
    int uiMode;
    QSize toolsIconSize, windowSize;
    bool windowMaximized;
    pgeneral *pageGeneral;
    pDrives *pageDrives;
    pBattery *pageBattery;
    pnetwork *pageNetwork;
    presources *pageResources;
    pSysMonitor *pageSysMonitor;
    pDisplay *pageDisplay;

    void pageClick(QToolButton *btn, int i, const QString& title);
    void loadSettings();
    void startSetup();
    void setupIcons();
};
