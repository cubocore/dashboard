/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QStringListModel>

#include <cprime/cprime.h>
#include <cprime/messageengine.h>

#include "pdrives.h"
#include "ui_pdrives.h"


pDrives::pDrives(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::pDrives)
	, sMgr(new StorageManager)
	, smi(new settings)
{
    ui->setupUi(this);
    ui->infoLeft->setStyleSheet("QLabel {background-color: transparent;}");
    ui->infoRight->setStyleSheet("QLabel {background-color: transparent;}");
    ui->driveView->setStyleSheet("QTreeView {background-color: transparent;}");
	QSize iconSize = smi->getValue("CoreApps", "ListViewIconSize");
	ui->driveView->setIconSize(iconSize);
    setupDisksPage();
    ui->mm->setText("Select a Block");

    ui->mount_2->setDisabled(true);
    ui->unmount_2->setDisabled(true);
}

pDrives::~pDrives()
{
	delete sMgr;
	delete smi;
    delete ui;
}

void pDrives::setupDisksPage()
{
    connect(sMgr, SIGNAL(deviceAdded(QString)), this, SLOT(reload()));
    connect(sMgr, SIGNAL(deviceRemoved(QString)), this, SLOT(reload()));

    reload();
}

void pDrives::on_driveView_itemSelectionChanged()
{
    if (ui->driveView->currentItem()) {
        updateInfo(ui->driveView->currentItem());
    }
}

void pDrives::updateInfo(QTreeWidgetItem *item)
{
    bool isDrive = item->data(1, Qt::UserRole).toBool();

    QStringList left;
    QStringList right;

    if (isDrive) {
        QString devPath = item->data(0, Qt::UserRole + 1).toString();
        QString drivePath(devPath.split("/").takeLast());
		StorageDevice drive(drivePath, sMgr);

        left.clear();
        right.clear();

        left << "Device";                 // Device will be Vendor + Model
        left << "Id";                     // Id of the device
        left << "Removable";              // Is removable device
        left << "Optical";                // Is optical device
        left << "Size";                   // Size of the device
        left << "Rotation Rate";
        left << "Seat";

        QDBusInterface iface("org.freedesktop.UDisks2", devPath, "org.freedesktop.UDisks2.Drive", QDBusConnection::systemBus());
        right << drive.label().trimmed();
        right << drive.id();
        right << (drive.isRemovable() ? "True" : "False") ;
        right << ((drive.property("MediaCompatibility").toStringList().filter("optical").length() > 0) ? "True" : "False");
        right << CPrime::FileUtils::formatSize(drive.size());
        right << QString::number(drive.rotationRate());
        right << drive.seat();

        // Update Mount option
        ui->mm->setText("Select a block");
        ui->mount_2->setEnabled(false);
        ui->unmount_2->setEnabled(false);
    } else {
        QString blockID = item->data(0, Qt::UserRole + 1).toString().split("/").takeLast();
        StorageBlock block(blockID);

        left.clear();
        right.clear();

        left << "Label";
        left << "Path";
        left << "Device";
        left << "Drive";
        left << "Mount Point";
        left << "File System";
        left << "Size";
        left << "Optical";
        left << "Removable";
        left << "Usage";

        right << block.label();
        right << block.path();
        right << block.device();
        right << block.drive();
        right << block.mountPoint();
        right << block.fileSystem();
        right << CPrime::FileUtils::formatSize(block.totalSize());
        right << (block.isOptical() ? "True" : "False");
        right << (block.isRemovable() ? "True" : "False");

		QString totalStr = CPrime::FileUtils::formatSize(block.totalSize());
		QString usageStr = CPrime::FileUtils::formatSize(block.totalSize() - block.availableSize());

		qreal percent = 100.0 * ((block.totalSize() - block.availableSize()) / block.totalSize());

        if (block.fileSystem() == "swap") {
            right << "";
        } else {
			right << QString("%1 of %2 (%3)%").arg(usageStr, totalStr).arg(percent);
        }

        // Update Mount and Unmount
        ui->mm->setText("Selected Block");

        /* Allow Mount/Unmount only for non swap partitions */
        if (block.fileSystem() != "swap") {
            /* Mounted: Allow unmount */
            if (block.mountPoint().length()) {
                ui->mount_2->setEnabled(false);
                ui->unmount_2->setEnabled(true);
            } else { /* Unmounted: Allow mount */
                ui->mount_2->setEnabled(true);
                ui->unmount_2->setEnabled(false);
            }
        }
    }

    ui->infoLeft->clear();
    ui->infoRight->clear();

    int count = left.length();
    QString sep = "\n";

    for (int i = 0; i < count; i++) {
        if (i + 1 == count) {
            sep = "";
        }

        ui->infoLeft->setText(ui->infoLeft->text() + left[i] + sep);
        ui->infoRight->setText(ui->infoRight->text() + ": " + right[i] + sep);
    }
}

void pDrives::reload()
{
    ui->driveView->clear();
    ui->driveView->setColumnCount(2);

    foreach (StorageDevice drive, sMgr->devices()) {
        auto itemDrive = new QTreeWidgetItem(ui->driveView);

        QString name = drive.label();

        if (name.isEmpty()) {
            name = drive.path().split("/").takeLast();
        }

        name = name.trimmed();

        QString size = CPrime::FileUtils::formatSize(drive.size());
        QIcon icon;

        if (drive.isOptical()) {
            icon = QIcon::fromTheme("drive-optical");
        } else if (drive.isRemovable()) {
            icon = QIcon::fromTheme("drive-removable-media");
        } else {
            icon = QIcon::fromTheme("drive-harddisk");
        }

        itemDrive->setIcon(0, icon);
        itemDrive->setText(0, name);
        itemDrive->setData(1, Qt::UserRole, true);
        itemDrive->setText(1, size);
        itemDrive->setData(0, Qt::UserRole + 1, drive.path());

        foreach (StorageBlock block, drive.partitions()) {

            if (block.fileSystem().isEmpty() and block.property("Partition", "Number").toInt() == 0) {
                continue;
            }

            if (block.property("Partition", "IsContainer").toBool()) {
                continue;
            }

            QString blockName = block.label();

            if (blockName.isEmpty()) {
                blockName = block.path().split("/").takeLast();
            }

                    blockName = blockName.trimmed();

            QString blockSize = CPrime::FileUtils::formatSize(block.totalSize());
            auto itemPartition = new QTreeWidgetItem(itemDrive);
            itemPartition->setIcon(0, QIcon::fromTheme("drive-partition"));
            itemPartition->setText(0, blockName);
            itemPartition->setText(1, blockSize);
            itemPartition->setData(1, Qt::UserRole, false);
            itemPartition->setData(0, Qt::UserRole + 1, block.path());
            itemDrive->addChild(itemPartition);
        }

        ui->driveView->addTopLevelItem(itemDrive);
    }

    ui->driveView->header()->resizeSections(QHeaderView::ResizeToContents);
}

void pDrives::on_mount_2_clicked()
{
    QString data = getSelectedBlockPath();

    if (!data.length()) {
        return;
    }

    QString blockID = data.split("/").takeLast();
    StorageBlock block(blockID);

    /* Try mounting anything that isn't swap */
    if (block.fileSystem() != "swap")
        if (block.mount()) {
			CPrime::MessageEngine::showMessage("cc.cubocore.CoreStats", "CoreStats", "Mounted", "Partition '" + block.device() + "' mounted.");
            ui->mount_2->setEnabled(false);
            ui->unmount_2->setEnabled(true);
        }
}

void pDrives::on_unmount_2_clicked()
{
    QString data = getSelectedBlockPath();

    if (!data.length()) {
        return;
    }

    QString blockID = data.split("/").takeLast();
    StorageBlock block(blockID);

    /* Try mounting anything that isn't swap */
    if (block.fileSystem() != "swap")
        if (block.unmount()) {
			CPrime::MessageEngine::showMessage("cc.cubocore.CoreStats", "CoreStats", "Unmounted", "Partition '" + block.device() + "' unmounted.");
            ui->mount_2->setEnabled(true);
            ui->unmount_2->setEnabled(false);
        }
}

QString pDrives::getSelectedBlockPath()
{
    QString path = "";
    QTreeWidgetItem *item = ui->driveView->currentItem();

    if (!item) {
        return path;
    }

    bool drive = item->data(1, Qt::UserRole).toBool();

    if (drive) {
        return path;
    }

    path = item->data(0, Qt::UserRole + 1).toString();

    return path;
}
