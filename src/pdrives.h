/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>
#include <QTreeWidgetItem>

#include <csys/storageinfo.h>

#include "settings.h"

namespace Ui {
	class pDrives;
}

class pDrives : public QWidget {
    Q_OBJECT

public:
    explicit pDrives(QWidget *parent = nullptr);
    ~pDrives();

private:
    Ui::pDrives     *ui;
	StorageManager  *sMgr;
    settings        *smi;

    void setupDisksPage();
    QString getSelectedBlockPath();

public slots:
    void reload();

private slots:
    void updateInfo(QTreeWidgetItem *item);
    void on_mount_2_clicked();
    void on_unmount_2_clicked();
    void on_driveView_itemSelectionChanged();
};
