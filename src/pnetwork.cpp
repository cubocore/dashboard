/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <cprime/filefunc.h>

#include "global.h"
#include "pnetwork.h"
#include "ui_pnetwork.h"


pnetwork::pnetwork(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::pnetwork)
{
    ui->setupUi(this);

    timer = new QBasicTimer;
    usageFile = new QSettings("coreapps", "networkusage");
    currDT = QDateTime::currentDateTime();

    timer->start(1000, this);
    updateNetwork();
    loadAllUsage();
}

pnetwork::~pnetwork()
{
	delete timer;
    delete usageFile;
    delete ui;
}

void pnetwork::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        updateNetwork();
    }

    event->accept();
}

void pnetwork::loadAllUsage()
{
    QString monthName = currDT.toString("MMMM");
    QMap<QString, QString> data;
    data.clear();

    QMap<QString, QString> daysData;
    daysData.clear();

    QListWidgetItem *item;

    QStringList keys;
	quint64 totalDownload = 0;
    quint64 totalUpload = 0;

    QStringList groups = usageFile->childGroups();

    Q_FOREACH (QString group, groups) {
        usageFile->beginGroup(group);

        keys.clear();
        keys = usageFile->childKeys();

		quint64 mDownload = 0;
		quint64 mUpload = 0;

		QStringList values;

        // Loop for all the available days
        Q_FOREACH (QString key, keys) {
            values.clear();

            // Get download and upload values
            values = usageFile->value(key).toStringList();

			int download = values[0].toULongLong();
			int upload = values[1].toULongLong();

            mDownload += download;
            mUpload += upload;

            // Store the day usage data
            daysData[key] = QString("Down: %1\tUp: %2").arg(CPrime::FileUtils::formatSize(download), CPrime::FileUtils::formatSize(upload));
        }

        // Store the total usage
        totalDownload += mDownload;
        totalUpload += mUpload;

        // Store the month usage data
        data[group] = QString("Down: %1\tUp: %2").arg(CPrime::FileUtils::formatSize(mDownload), CPrime::FileUtils::formatSize(mUpload));

        usageFile->endGroup();
    }

    // Setting group box title
    ui->boxMonths->setTitle("Total: Down- " + CPrime::FileUtils::formatSize(totalDownload) + ", Up- " + CPrime::FileUtils::formatSize(totalUpload));
    ui->boxDays->setTitle(monthName + ": " + data[monthName]);

    // Add all months usage
    QString month;
    QStringList monthsList = data.keys();

    for (int i = 0; i < monthsList.size(); i++) {
        month = monthsList[i];
        item = new QListWidgetItem(month + "\t" + data[month]);
        ui->months->addItem(item);
    }

    // Add all the days from current month
    QString day;
    QStringList dayList = daysData.keys();

    for (int i = 0; i < daysData.count(); i++) {
        day = dayList.at(i);
        item = new QListWidgetItem(day + " \t" + daysData[day]);
        ui->days->addItem(item);
    }
}

void pnetwork::updateNetwork()
{
    QList<QList<double>> IOBytes = infoMgr->getNetworkUsage(1);
    QList<QList<double>> IOSpeed = infoMgr->getNetworkSpeed(1);

    ui->today->setText("<p><sub>TODAY  </sub>"+ CPrime::FileUtils::formatSize(IOBytes[0][0]) +"<sup>DN</sup></p><p>   "+CPrime::FileUtils::formatSize(IOBytes[1][0])+"<sub>UP</sub></p>");
    ui->now->setText("<p><sub>NOW  </sub>"+ CPrime::FileUtils::formatSize(IOSpeed[0][0]) +"<sup>DN</sup></p><p>   "+CPrime::FileUtils::formatSize(IOSpeed[1][0])+"<sub>UP</sub></p>");
}
