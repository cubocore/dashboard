/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QVBoxLayout>

#include <cprime/filefunc.h>
#include <csys/diskinfo.h>
#include <csys/cpuinfo.h>

#include "global.h"
#include "usagegraph.h"
#include "presources.h"
#include "ui_presources.h"

presources::presources(QWidget *parent) : QWidget(parent)
{
	mTimer = new QBasicTimer();

//	infoMgr->resetAllUsages();

	init();
}

presources::~presources()
{
	delete mTimer;
}

void presources::init()
{
	QVBoxLayout *chartsLayout = new QVBoxLayout();

	/* n cores */
	mPlotCpu = new UsageGraph("CPU Load", "%", infoMgr->getCpuCoreCount(), this);

	/* Ram and Swap */
    mPlotMemory = new UsageGraph("Memory Usage", "GiB", 2, this);
    mPlotMemory->setYRange(0, qMax(infoMgr->getMemTotal(), infoMgr->getSwapTotal()));

	/* 1 minute, 5 minutes,, 15 minutes */
    mPlotCpuLoadAvg = new UsageGraph("CPU Load Averages", "%", 3, this);
    mPlotCpuLoadAvg->setLegend(QStringList() << "15 Minutes" << "5 Minutes" << "1 Minute");

	/* Read and Write */
    DiskInfo disk;

    mPlotDiskIO = new UsageGraph(QString("Disk Read/Write(%1)").arg(disk.getRootFolderDisk()), "B", 2, this);
    mPlotDiskIO->setLegend(QStringList() << "Read: 0 B" << "Write: 0 B");
    mPlotDiskIO->setYRange(0, 100);

	/* Upload and Download */
    mPlotNetwork = new UsageGraph("Network Speed", "B/s", 2, this);
    mPlotNetwork->setLegend(QStringList() << "Download: 0 B" << "Upload: 0 B");
    mPlotNetwork->setYRange(0, 100);

    chartsLayout->addWidget(mPlotCpu);
    chartsLayout->addWidget(mPlotMemory);
    chartsLayout->addWidget(mPlotCpuLoadAvg);
    chartsLayout->addWidget(mPlotDiskIO);
    chartsLayout->addWidget(mPlotNetwork);

    setLayout(chartsLayout);

    connect(mPlotCpu, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotCpuLoadAvg, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotDiskIO, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotMemory, SIGNAL(zoomChart()), this, SLOT(zoomChart()));
    connect(mPlotNetwork, SIGNAL(zoomChart()), this, SLOT(zoomChart()));

	/* 1 second timer */
    mTimer->start(500, this);
}

void presources::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == mTimer->timerId()) {
        /* Update the charts */
        updateCpuChart();
        updateMemoryChart();
        updateCpuLoadAvg();
        updateDiskReadWrite();
        updateNetworkChart();
	}

	tEvent->accept();
}

void presources::updateCpuChart()
{
	QList<UsageValues> percents = infoMgr->getCpuPercents();

	QStringList cpuLegends;
	for (int i = 0; i < infoMgr->getCpuCoreCount(); i++) {
        cpuLegends << QString("CPU %1: %2%").arg(i).arg((int)(percents[ i ].last()));
	}

	mPlotCpu->updateValues(percents, infoMgr->getCpuCoreCount());
    mPlotCpu->setLegend(cpuLegends);
}

void presources::updateMemoryChart()
{
    UsageValues memUsage = infoMgr->getMemUsed();
    UsageValues swapUsage = infoMgr->getSwapUsed();

	QStringList legends;

    quint64 totalRam = infoMgr->getMemTotal();
    quint64 totalSwap = infoMgr->getSwapTotal();

	// Memory
    if (totalRam) {
		legends << QString("RAM: %1 of %2").arg(CPrime::FileUtils::formatSize(memUsage.last()), CPrime::FileUtils::formatSize(totalRam));
    } else {
        legends << QString("RAM: 0 b of 0 b");
	}

	// Swap
    if (totalSwap) {
		legends << QString("Swap: %1 of %2").arg(CPrime::FileUtils::formatSize(swapUsage.last()), CPrime::FileUtils::formatSize(totalSwap));
    } else {
        legends << QString("Swap: 0 b of 0 b");
	}

    mPlotMemory->updateValues({memUsage, swapUsage});
    mPlotMemory->setLegend(legends);
}

void presources::updateCpuLoadAvg()
{
	QList<UsageValues> cpuLoadAvgs = infoMgr->getCpuLoadAvgs();

	double max1 = *std::max_element(cpuLoadAvgs[0].begin(), cpuLoadAvgs[0].end());
	double max2 = *std::max_element(cpuLoadAvgs[1].begin(), cpuLoadAvgs[1].end());
	double max3 = *std::max_element(cpuLoadAvgs[2].begin(), cpuLoadAvgs[2].end());
	double max = CPrime::FileUtils::maxYForSize(qMax(qMax(max1, max2), max3));

	mPlotCpuLoadAvg->setYRange(0, max);
	mPlotCpuLoadAvg->updateValues(cpuLoadAvgs);
}

void presources::updateDiskReadWrite()
{
    QList<UsageValues> diskSpeeds = infoMgr->getDiskSpeed();

    double rMax = *std::max_element(diskSpeeds[ 0 ].begin(), diskSpeeds[ 0 ].end());
    double wMax = *std::max_element(diskSpeeds[ 1 ].begin(), diskSpeeds[ 1 ].end());
    double max = CPrime::FileUtils::maxYForSize(qMax(rMax, wMax));

    QStringList legend;

    legend << QString("Read: %1").arg(CPrime::FileUtils::formatSize(diskSpeeds[ 0 ].last()));
    legend << QString("Write: %1").arg(CPrime::FileUtils::formatSize(diskSpeeds[ 1 ].last()));

    mPlotDiskIO->setLegend(legend);
    mPlotDiskIO->setYRange(0, max);

    mPlotDiskIO->updateValues(diskSpeeds);
}

void presources::updateNetworkChart()
{
    QList<UsageValues> netSpeeds = infoMgr->getNetworkSpeed();

    double dlMax = *std::max_element(netSpeeds[ 0 ].begin(), netSpeeds[ 0 ].end());
    double ulMax = *std::max_element(netSpeeds[ 1 ].begin(), netSpeeds[ 1 ].end());
    double max = CPrime::FileUtils::maxYForSize(qMax(dlMax, ulMax));

    QStringList legend;

    legend << QString("Download: %1 ").arg(CPrime::FileUtils::formatSize(netSpeeds[ 0 ].last()));
    legend << QString("Upload: %1 ").arg(CPrime::FileUtils::formatSize(netSpeeds[1].last()));

    mPlotNetwork->setLegend( legend );
    mPlotNetwork->setYRange(0, max);
    mPlotNetwork->updateValues(netSpeeds);
}

void presources::zoomChart()
{
    UsageGraph *chart = qobject_cast<UsageGraph *>(sender());

    /* Show one graph zoomed */
    if (chart->zoom()) {
        mPlotCpu->setVisible(mPlotCpu == chart);
        mPlotCpuLoadAvg->setVisible(mPlotCpuLoadAvg == chart);
        mPlotDiskIO->setVisible(mPlotDiskIO == chart);
        mPlotMemory->setVisible(mPlotMemory == chart);
        mPlotNetwork->setVisible(mPlotNetwork == chart);
    } else {
		mPlotCpu->show();
		mPlotCpuLoadAvg->show();
		mPlotDiskIO->show();
		mPlotMemory->show();
		mPlotNetwork->show();
	}
}
